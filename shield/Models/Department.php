<?php

namespace acsp\shield\Models;

class Department extends \HiMax\model\Department {
    protected $database = 'painel';
    protected $table = 'PNDEPART';
    protected $primaryKey = 'id';
    protected $deactivate = \HiMax\TABLES_FIELD_DELETE;
    
    protected $foreignKeys = [
    ];

    protected $fieldsAliases = [
        'nome' => 'name',
        'ref_id' => 'id',
    ];
    
    public function findAll($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $results = parent::findAll($conditions, $limit, $page, $columns, $orderby, $recursive);
        if(!empty($results)) foreach($results as $x => $y) {
            $results[$x]['id'] = trim($results[$x]['id']);
            $results[$x]['name'] = trim($results[$x]['name']);
        }
        return $results;
    }
}
