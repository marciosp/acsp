<?php

namespace acsp\shield\Models;

class Action extends \HiMax\model\Action {
    protected $database = 'painel';
    protected $table = 'PNACTION';
    
    protected $fieldsAliases = [
        'access' => 'security_level',
        'sistema_id' => 'system_id',
//        'module' => 'module',
        'controller' => 'class',
//        'action' => 'action',
//        'url' => 'url',
    ];
    
    protected $foreignKeys = [
        'system' => [
            'type' => \doctrine\Dashes\BELONGSTO,
            'key' => 'system_id',
            'model' => '\acsp\shield\Models\System'
        ],
    ];
    
    public $access_public = 'public';
    public $access_internal = 'internal';
    public $access_private = 'private';
    
    protected $SystemAclControl = true;
    protected $ignoreSystemFilter = false;
}
