<?php

namespace acsp\shield\Models;

class Access extends \HiMax\model\Access {
    protected $database = 'painel';
    protected $table = 'PNACCESS';
    protected $foreignKeys = [
        'action' => [
            'type' => \doctrine\Dashes\BELONGSTO,
            'key' => 'action_id',
            'model' => '\acsp\shield\Models\Action'
        ],
    ];
    
    protected $fieldsAliases = [
        'sistema_id' => 'system_id',
        'perfil_id' => 'profile_id',
    ];
}
