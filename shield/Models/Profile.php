<?php

namespace acsp\shield\Models;

class Profile extends \HiMax\model\Profile {

    protected $database = 'painel';
    protected $table = 'PNPERFIL';
    protected $fieldsAliases = [
        'nome' => 'name',
        'active' => 'status',
        'sistema_id' => 'system_id',
        'departamento_id' => 'department_id',
        'dt_criacao' => 'created',
        'dt_update' => 'updated',
    ];
    protected $foreignKeys = [
        'access' => [
            'type' => \doctrine\Dashes\HASMANY,
            'key' => 'profile_id',
            'model' => '\acsp\shield\Models\Access'
        ],
        'system' => [
            'type' => \doctrine\Dashes\BELONGSTO,
            'key' => 'system_id',
            'model' => '\acsp\shield\Models\System'
        ],
    ];
    
    protected $SystemAclControl = true;
    protected $ignoreSystemFilter = false;

}
