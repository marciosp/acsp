<?php

date_default_timezone_set('America/Sao_Paulo');

/**
 * Detecção de ambientes através do hostname
 * 
 * */
if ($_SERVER['HTTP_HOST'] === 'localhost') {
    $environment = 'development';
} elseif (preg_match('/^t\w+\.(vixs)\.com\.br/', $_SERVER['HTTP_HOST'])) {
    $environment = 'staging';
} else {
    switch ($host = @exec("hostname")) {
        case 'WebServerPro':
        case 'srv-dc-portal':
        case 'wsp':
        case 'wsp_2':
            $environment = 'production';
            break;
        case 'webserverdev':
        case 'wsd':
            $environment = 'staging';
            break;
        default:
            $environment = 'development';
//		$environment = 'staging'; // utilizado em testes locais
            break;
    }
}

//echo '<!--' . $host .'_'. $environment . '-->';
!defined('ENVIRONMENT') && define('ENVIRONMENT', $environment);

return $environment;
