<?php

namespace acsp\helpers\core;

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller_rf extends \Restserver\Libraries\REST_Controller {

    use CBasis,
        \codeigniter\CodeBlaze\spa_rf {
            CBasis::renderLayout insteadof \codeigniter\CodeBlaze\spa_rf;
            CBasis::getAttrProperty insteadof \codeigniter\CodeBlaze\spa_rf;
            CBasis::sendJsonMsg insteadof \codeigniter\CodeBlaze\spa_rf;

            \codeigniter\CodeBlaze\spa_rf::renderLayout as CodeBlaze_renderLayout;
    }

    public function __construct($loadModel = true) {
        $this->CB__construct($loadModel);
    }

}
