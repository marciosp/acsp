<?php

namespace acsp\helpers\core;

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Model extends \CI_Model {

    use \acsp\helpers\core\Model;

    public function __construct() {
        parent::__construct();
        $this->load->database();
    }

}
