<?php

namespace acsp\helpers\core;

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Public_Controller extends \CI_Controller {
    
    use PCBasis,
        \codeigniter\CodeBlaze\Controller {
            PCBasis::renderLayout insteadof \codeigniter\CodeBlaze\Controller;
            PCBasis::getAttrProperty insteadof \codeigniter\CodeBlaze\Controller;
            PCBasis::sendJsonMsg insteadof \codeigniter\CodeBlaze\Controller;
        }
    
    public function __construct($loadModel = true) {
        $this->PCB__construct($loadModel);
    }

    /* legacy */
    public function delete($id = NULL) {
        return $this->remove($id);
    }


}