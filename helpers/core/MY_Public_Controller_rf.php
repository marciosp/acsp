<?php

namespace acsp\helpers\core;

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Public_Controller_rf extends \Restserver\Libraries\REST_Controller {
    
    use PCBasis,
        \codeigniter\CodeBlaze\spa_rf {
            PCBasis::renderLayout insteadof \codeigniter\CodeBlaze\spa_rf;
            PCBasis::getAttrProperty insteadof \codeigniter\CodeBlaze\spa_rf;
            PCBasis::sendJsonMsg insteadof \codeigniter\CodeBlaze\spa_rf;
        }
    
    public function __construct($loadModel = true) {
        $this->PCB__construct($loadModel);
    }


}