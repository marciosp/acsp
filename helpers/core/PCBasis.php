<?php

namespace acsp\helpers\core;

/* Public Controller Basis */

trait PCBasis {

    use CBasis;
    
    protected $pcbasisAttrDefaults = [
        'layout' => '/site/index'
    ];

    public function getAttrProperty() {
        $defaults = array_merge($this->attrDefaults, $this->cbasisAttrDefaults, $this->pcbasisAttrDefaults);
        return $defaults;
    }

    public function PCB__construct($loadModel = true) {
        parent::__construct();

        $loadModel && ($this->Model = $this->loadModel(!property_exists($this, 'modelPath') ? get_class($this) . '_model' : $this->modelPath));

        $this->load->helper('url');
        $this->load->library('session');
    }

    protected function renderLayout($layout, $data = []) {
        return $this->load->view($layout, $data, true);
    }

}
