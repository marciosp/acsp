<?php

namespace acsp\helpers\core;

trait LoadModel {
    
    /**
     * Load model
     * @param string $model
     * @return object instance
     */
    public function loadModelInstance($model, $setDbConfig=true) {
        $ci = \get_instance();
        $util = !empty($ci->config->config['utils']) ? $ci->config->config['utils']() : (new \acsp\helpers\Util);
        
        $modelShortName = \Crush\Basic::getClassShortName($model);
        if (!(@$this->model[$modelShortName]) || !is_object(@$this->model[$modelShortName])) {
            $modelObj = $util::loadDashesModel($model);
            $this->model[$modelShortName] = $modelObj;
        }

        return $this->model[$modelShortName];
    }

}
