<?php

namespace acsp\helpers;

/**
 * 
 * @author      
 * */
class Assoc {
    
    public static $facespConsumidor = '60524550000131';
    public static $facespSenha = '8ABD4567';
    
    public static function getACSP($cnpj) {
        $cnpj = preg_replace('/\D+/','', $cnpj);
        
        $api = new \acsp\helpers\Api();
        $api->doAuth();
        $url = \acsp\helpers\Url::ambienteUrl("api.acspservicos.com.br/common/protheus_member/MemberClient/ACSP/0/" . $cnpj);

        $method = 'GET';
        $response = $api->getUrl($url, $method)->getResponse()->response;

        if(is_array($response) && count($response) === 1) {
            $item = array_shift($response);
            if(!empty($item['A1_CODCOS'])) {
                return $item;
            }
        }
        return [];
    }
    
    public static function getFACESP($cnpj) {
        $cnpj = preg_replace('/\D+/','', $cnpj);
        
        $data = [
            'consumidor' => static::$facespConsumidor,
            'senha' => static::$facespSenha
        ];
        
        $data['documento'] = $cnpj;
        $data['TipoDocumento'] = strlen($data['documento']) > 11 ? '2' : '1';

//        $api = \acsp\helpers\Api::post('http://ww2.facesp.com.br/servico/wsfacesp/facespws.asmx/VerificaAssociadoFACESP', $data, '', '', false);
        $api = \acsp\helpers\Api::post('http://ww2.facesp.com.br/servico/wsfacesp/facespws.asmx/VerificaAssociado', $data, '', '', false);
        $xml = @simplexml_load_string($api->_raw);

        if($xml) {
            $cod = (string)$xml->Mensagem->Codigo;
            $assoc = (string)$xml->Associado->DocumentoEntidade;

//            $cod === '00' && $msg === 'S' && ($isAssociate = true);
            if($cod === '00' && (string) $assoc !== '99999999') {
                return ['A1_CODCOS'=>$assoc];
            }
        }
        
        return [];
    }
}
