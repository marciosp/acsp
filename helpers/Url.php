<?php

namespace acsp\helpers;

/**
 * @author Bruno Foggia
 */
class Url {

    /**
     * 
     * @param type $useSegments posiçao do segmento da url ate o qual serao capturados os segmentos para composicao da url (0=>modulo/,1=>modulo/controller,2=modulo/controller/metodo)
     * @param type $after continuacao da url que sera composta
     */
    public function compose($useSegments = 0, $after = '/') {
        global $URI;

        $c = -1;
        $url = '/';
        while ($c++ < $useSegments) {
            $url .= $URI->segment($c) . '/';
        }

        $url = rtrim($url, '/') . $after;

        return $url;
    }

    public static function ambienteUrl($url, $forceProduction=false) {
        $scheme = $_SERVER['REQUEST_SCHEME'] . '://';
        if(preg_match('/^\w+\:\/\//', $url, $matches)) {
            $scheme = $matches[0];
            $url = explode($scheme, $url, 2)[1];
        }
        
        if(!$forceProduction) {
            switch (ENVIRONMENT) {
                case 'development':
                    $scheme .= 'd';
                    break;
                case 'staging':
                    $scheme .= 't';
                    break;
                case 'production':
                    break;
            }
        }

        $url = $scheme . $url;

        return $url;
    }
    
    public static function dir($relative = true) {
        if($relative) {
            return substr(__DIR__, strpos(__DIR__, 'vendor/'));
        }
        return __DIR__;
    }

}
