<?php

namespace acsp\helpers;

/**
 * api_call
 * 
 * Realiza chamadas para a API-DEFAULT;
 * 
 * @author      Renan Correa Pinto - FCamara
 * @package 	Codeigniter 2.x
 * @version     28/08/2014 - 1.0
 * */
class Api {

    /**
     * Request and Response Parameters
     * */
    public $_request_header;
    public $_raw;
    public $_response;
    public $_response_header;
    public $_response_protocol;
    public $_response_code;
    public $_response_code_text;

    /**
     * Default API root passwords
     * */
    protected $_username = ACSP_WS_GLOBALUSER;
    protected $_password = ACSP_WS_GLOBALPASS;

    /**
     * Internal controllers
     * */
    protected $_url;
    protected $_auth;

    public function __construct(Array $params = array()) {
        if (isset($params['url'])) {
            $this->_url = $params['url'];
        }
    }

    /**
     * Define que a chamada utilizará autenticação
     * */
    public function doAuth() {
        $this->_auth = true;
        return $this;
    }

    /**
     * Define outro usuario para requisição
     * */
    public function setAuth($username = "", $password = "") {
        if (!empty($username))
            $this->_username = $username;
        if (!empty($password))
            $this->_password = $password;
        $this->_auth = true;

        return $this;
    }

    /**
     * Executa determinada URL via cURL;
     * */
    public function call($url, $method = 'GET', $request_parameters = null, $request_headers = array()) {
        // Verifica se os parametros estao setados, se sim devem ser em formato jSON;
        $this->_url = '';
        $request_post = '';
        $content_type = '';
        if (!empty($request_parameters)) {
            if (!$this->isJson($request_parameters)) {
                if (is_array($request_parameters)) {
                    $request_query = http_build_query($request_parameters);
                    $request_json = json_encode($request_parameters);
                } else
                    return false;
            } else {
                $request_query = $request_parameters;
                $request_parameters = null;
                $content_type = 'application/json';
            }
        }

        if (!empty($this->_url))
            $url = $this->_url . $url;

        $default_headers = array();
        $content_type && $default_headers['content-type'] = 'Content-Type: ' . $content_type . '; charset=utf-8';
        $content_type && $default_headers[] = 'Accept: ' . $content_type;

        // Configurando cURL;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);                //header de resposta
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);        //header de requisição

        if ($this->_auth) {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $this->_username . ':' . $this->_password);
        }

        // Identifica o método;
        switch ($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                break;
            case 'PUT': curl_setopt($ch, CURLOPT_PUT, 1);
                break;
            case 'DELETE': curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
        }

        // Inserindo Parametros da Requisição;
        if (!empty($request_query)) {
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request_query);
        }

        // Configurando Headers da Requisição;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($default_headers, $request_headers));


        // Executando cURL;
        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $this->_request_header = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        $this->_response_header = substr($response, 0, $header_size);
        $this->_response_protocol = substr($this->_response_header, 0, 8);
        $this->_response_code = substr($this->_response_header, 9, 3);
        $this->_response_code_text = $this->get_response_code_text($this->_response_code);

        $response = substr($response, $header_size);

        $this->_raw = empty($response) ? curl_error($ch) : $response;
        $this->_response = ($this->isJson($response) ? json_decode($response, 1) : false);

        return $this;
    }

    /**
     * Executa determinada URL via cURL;
     * */
    public function getUrl($url, $method = 'GET', $request_parameters = null, $request_headers = array()) {
        // Verifica se os parametros estao setados, se sim devem ser em formato jSON;
        $request_post = '';
        if (!empty($request_parameters)) {
            if (!$this->isJson($request_parameters)) {
                if (is_array($request_parameters)) {
                    $request_query = http_build_query($request_parameters);
                    $request_json = json_encode($request_parameters);
                } else
                    return false;
            } else {
                $request_query = ($request_parameters);
            }
        }

        if (!empty($this->_url) && strpos($url, 'http')===false)
            $url = $this->_url . $url;

        $default_headers = array();
        $content_type = 'application/json';
        $default_headers['content-type'] = 'Content-Type: ' . $content_type . '; charset=utf-8';
        $default_headers[] = 'Accept: ' . $content_type;

        // Configurando cURL;
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_VERBOSE, 1);
        curl_setopt($ch, CURLOPT_HEADER, 1);                //header de resposta
        curl_setopt($ch, CURLINFO_HEADER_OUT, true);        //header de requisição

        if ($this->_auth) {
            curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($ch, CURLOPT_USERPWD, $this->_username . ':' . $this->_password);
        }

        // Identifica o método;
        switch ($method) {
            case 'POST':
                curl_setopt($ch, CURLOPT_POST, 1);
                break;
            case 'PUT': curl_setopt($ch, CURLOPT_PUT, 1);
                break;
            case 'DELETE': curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
                break;
        }

        // Inserindo Parametros da Requisição;
        if (!empty($request_parameters)) {
            unset($default_headers['content-type']);
            curl_setopt($ch, CURLOPT_POSTFIELDS, $request_query);
        }

        // Configurando Headers da Requisição;
        curl_setopt($ch, CURLOPT_HTTPHEADER, array_merge($default_headers, $request_headers));


        // Executando cURL;
        $response = curl_exec($ch);
        $header_size = curl_getinfo($ch, CURLINFO_HEADER_SIZE);
        $this->_request_header = curl_getinfo($ch, CURLINFO_HEADER_OUT);

        $this->_response_header = substr($response, 0, $header_size);
        $this->_response_protocol = substr($this->_response_header, 0, 8);
        $this->_response_code = substr($this->_response_header, 9, 3);
        $this->_response_code_text = $this->get_response_code_text($this->_response_code);

        $response = substr($response, $header_size);

        $this->_raw = empty($response) ? curl_error($ch) : $response;
        $this->_response = ($this->isJson($response) ? json_decode($response, 1) : false);

        return $this;
    }

    /**
     * Verifica se uma string é um JSON válido.
     * */
    public function isJson($value) {
        if (is_array($value))
            return false;
        json_decode($value);
        return (json_last_error() == JSON_ERROR_NONE);
    }

    /**
     * Retorna parametros de comunicacao da ultima requisicao
     * */
    public function getResponse() {
        $response = new \stdClass();
        $response->raw = $this->_raw;
        $response->response = $this->_response;
        $response->protocol = $this->_response_protocol;
        $response->status = $this->_response_code;
        $response->message = $this->_response_code_text;
        $response->response_header = $this->_response_header;
        $response->request_header = $this->_request_header;

        // tratando erro		
        $response->error = 0;
        if (!$response->response) {
            $response->error = $this->_raw;
        }
        if (isset($response->response['status']) && $response->response['status'] == 'error') {
            $response->error = $response->response['message'];
        }

        return $response;
    }

    /**
     * HTTP Default response code list.
     * */
    public function get_response_code_text($code) {
        switch ($code) {
            case 100: $text = 'Continue';
                break;
            case 101: $text = 'Switching Protocols';
                break;
            case 200: $text = 'OK';
                break;
            case 201: $text = 'Created';
                break;
            case 202: $text = 'Accepted';
                break;
            case 203: $text = 'Non-Authoritative Information';
                break;
            case 204: $text = 'No Content';
                break;
            case 205: $text = 'Reset Content';
                break;
            case 206: $text = 'Partial Content';
                break;
            case 300: $text = 'Multiple Choices';
                break;
            case 301: $text = 'Moved Permanently';
                break;
            case 302: $text = 'Moved Temporarily';
                break;
            case 303: $text = 'See Other';
                break;
            case 304: $text = 'Not Modified';
                break;
            case 305: $text = 'Use Proxy';
                break;
            case 400: $text = 'Bad Request';
                break;
            case 401: $text = 'Unauthorized';
                break;
            case 402: $text = 'Payment Required';
                break;
            case 403: $text = 'Forbidden';
                break;
            case 404: $text = 'Not Found';
                break;
            case 405: $text = 'Method Not Allowed';
                break;
            case 406: $text = 'Not Acceptable';
                break;
            case 407: $text = 'Proxy Authentication Required';
                break;
            case 408: $text = 'Request Time-out';
                break;
            case 409: $text = 'Conflict';
                break;
            case 410: $text = 'Gone';
                break;
            case 411: $text = 'Length Required';
                break;
            case 412: $text = 'Precondition Failed';
                break;
            case 413: $text = 'Request Entity Too Large';
                break;
            case 414: $text = 'Request-URI Too Large';
                break;
            case 415: $text = 'Unsupported Media Type';
                break;
            case 500: $text = 'Internal Server Error';
                break;
            case 501: $text = 'Not Implemented';
                break;
            case 502: $text = 'Bad Gateway';
                break;
            case 503: $text = 'Service Unavailable';
                break;
            case 504: $text = 'Gateway Time-out';
                break;
            case 505: $text = 'HTTP Version not supported';
                break;
            default:
                $text = 'Unknown http status code "' . htmlentities($code) . '"';
                break;
        }
        return $text;
    }

    public static function post($url, $data = array(), $user = '', $pass = '', $ambienteUrl = true) {
        $api = new static(array('url' => ''));
        if (!empty($user)) {
            $api->setAuth($user, $pass);
        }

        if ($ambienteUrl) {
            $url = \acsp\helpers\Url::ambienteUrl($url);
        }
        $api->getUrl($url, 'POST', $data);
        return $api;
    }

    public static function postPainel($url, $data = array(), $user = '', $pass = '', $ambienteUrl = true) {
        $api = new static(array('url' => ''));

        $token = urlencode(\acsp\helpers\Auth::getWebserviceToken($api->_username, $api->_password));

        $url .= (strpos($url, '?') !== false ? '&' : '?') . 'token=' . $token;
        return static::post($url, $data);
    }

    public static function get($url, $data = array(), $user = '', $pass = '', $ambienteUrl = true) {
        $api = new static(array('url' => ''));
        if (!empty($user)) {
            $api->setAuth($user, $pass);
        }

        if ($ambienteUrl) {
            $url = Url::ambienteUrl($url);
        }
        $api->getUrl($url, 'GET', $data);
        return $api;
    }

    public function getJsonResponse() {
        $response = json_decode($this->_raw, true);
        return $response;
    }

}
