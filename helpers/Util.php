<?php

namespace acsp\helpers;

/**
 * @author Bruno Foggia
 */
class Util {

    public static function getDbConfig() {
        $ci = \get_instance();
        $ci->load->database();
        $db = \acsp\shield\Vendor::getVendorDbList()[$ci->db->dbconfig];
        $db['dbname'] = $ci->db->database;

        return $db;
    }
    
    public static function setDashesDbList($class, $defaultDB='') {
        foreach (\acsp\shield\Vendor::getVendorDbList() as $dbconfigname => $dbconfig) {
            !empty($defaultDB) && $defaultDB===$dbconfigname && $class::setConnConfig('', $dbconfig);
            $class::setConnConfig($dbconfigname, $dbconfig);
        }
    }

    public static function loadDashesModel($class, $setDefault=true) {
        $setDefault && $db = self::getDbConfig();
        if (method_exists($class, 'setConnConfig')) {
            $setDefault && $class::setConnConfig('', $db);
            static::setDashesDbList($class);
        }
        return new $class;
    }

    public static function getShortName($class) {
        $name = !is_object($class) ? $class : get_class($class);
        return array_reverse(explode('\\', $name))[0];
    }

}
