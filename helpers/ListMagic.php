<?php

namespace acsp\helpers;

/**
 * @author Bruno Foggia
 */
class ListMagic {

    /**
     * Cria uma lista customizada a partir de um array de dados
     * @param array $data
     * @param string $field1 nome do campo que sera a chave identificadora de cada registro do novo array
     * @param mixed $field2 (string/array) nome do(s) campo(s) que estarao contidos em cada item do novo array
     * @param mixed $options
     * @return array
     */
    public static function build($data, $field1, $field2 = [], $options = null) {
        if (!is_array($data)) {
            return [];
        }
        !is_array($options) && $options = array($options);

        $list = [];
        $header = [];
        foreach ($data as $index1 => $row) {
            $value1 = !empty($field1) && $field1 !== '=' ? static::_getListValue($field1, $row, $options) : ($field1 === '=' ? $index1 : $field1);
            is_array($field2) && count($field2) === 0 ? ($field2 = array_keys($row)) : null;

            if (is_array($field2)) {
                foreach ($field2 as $field) {
                    $field = explode(' ', $field, 2);
                    $value = static::_getListValue($field[0], $row, (array) $options);
                    $alias = (count($field) > 1) ? $field[1] : $field[0];

                    $value2[$alias] = static::_filterValues($field, $value, $options);

                    !in_array($alias, $header) && $header[] = $alias;
                }
            }

            $result = static::_applyItemFilters($value2, $options);

            if (in_array('removeEmpty', $options) && empty($result)) {
                continue;
            }

            $key = (!empty($value1) || (string) $value1 === '0') ? $value1 : count($list);
            if ((!is_int($key) || !empty($field1)) && in_array('array', $options)) {
                !array_key_exists($key, $list) && ($list[$key] = []);
                $list[$key][] = $result;
            } else {
                $list[$key] = $result;
            }
        }

        return static::_applyListFilters($list, $header, $options);
    }

    /**
     * 
     */
    protected static function _filterValues($field, $value, Array $options) {
        if (in_array('trim', $options)) {
            if (!is_array($value))
                $value = trim($value);
            else
                foreach ($value as $x => $y)
                    $value[$x] = trim($y);
        }

        if (array_key_exists('lists', $options) && !empty($options['lists']) && !is_array($value) && is_array($field) && !empty($options['lists'][$field[0]])) {
            $value = $options['lists'][$field[0]][$value];
        }

        if (array_key_exists('callbacks', $options) && !empty($options['callbacks']) && !is_array($value) && is_array($field) && !empty($options['callbacks'][$field[0]])) {
            $value = $options['callbacks'][$field[0]]($value);
        }

        return $value;
    }

    /**
     * 
     */
    protected static function _applyItemFilters($result, Array $options) {
        if (in_array('arrayValues', $options) && is_array($result)) {
            $result = array_values($result);
        }

        if (in_array('flatten', $options) && is_array($result)) {
            $result = array_shift($result);
        }

        return $result;
    }

    /**
     * 
     */
    protected static function _applyListFilters($list, $header, Array $options) {
        if (in_array('header', $options)) {
            array_unshift($list, $header);
        }
        if (in_array('header', array_keys($options), true)) {
            $header = array_map(function ($item) use ($options) {
                return in_array($item, array_keys($options['header']), true) ? $options['header'][$item] : $item;
            }, $header);
            array_unshift($list, $header);
        }
        if (in_array('interlace', $options, true)) {
            $nlist = [];
            foreach ($list as $x => $item) {
                if (!is_array($item))
                    $nlist[] = $item;
                else
                    (!empty($item) && ($nlist = static::array_interlace($nlist, $item)));
            }
            $list = $nlist;
        }
        return $list;
    }

    /**
     * 
     */
    protected static function _getListValue($key, $data, Array $options) {
        if (is_array($key) || is_object($key)) {
            return $key;
        } elseif (empty($key)) {
            return $data;
        }

        $keys = explode('+', $key);
        $keysValues = [];
        foreach($keys as $key) {
            $fields = explode('.', $key);
            $extractedData = (array) $data;
            foreach ($fields as $x => $field) {

                if ($field === '[]' && !array_key_exists($field, $extractedData)) {
                    return ListMagic::build($extractedData, '', [implode('.', array_slice($fields, $x + 1))], (array) @$options['under']);
                } else if (!array_key_exists($field, $extractedData)) {
                    return NULL;
                }

                $extractedData = $extractedData[$field];
                !is_array($extractedData) && ($keysValues[] = $extractedData);
            }
        }

        return !empty($keysValues) ? implode('', $keysValues) : $extractedData;
    }

    /**
     * pass any number of arrays and it will interlace and key them properly
     * @reference http://php.net/manual/pt_BR/function.array-merge.php#85029
     * @return boolean
     */
    public static function array_interlace() {
        $args = func_get_args();
        $total = count($args);

        if ($total < 2) {
            return FALSE;
        }

        $i = 0;
        $j = 0;
        $arr = array();

        foreach ($args as $arg) {
            foreach ($arg as $v) {
                $arr[$j] = $v;
                $j += $total;
            }

            $i++;
            $j = $i;
        }

        ksort($arr);
        return array_values($arr);
    }

}
