<?php

namespace acsp\helpers;

/**
 * Utilidades para trabalhar com datas, dias uteis e feriados
 * @author Bruno Foggia
 */
class Datas {
    
    public static $meses = [
        1 => ['Jan','Janeiro'],
        2 => ['Fev','Fevereiro'],
        3 => ['Mar','Março'],
        4 => ['Abr','Abril'],
        5 => ['Mai','Maio'],
        6 => ['Jun','Junho'],
        7 => ['Jul','Julho'],
        8 => ['Ago','Agosto'],
        9 => ['Set','Setembro'],
        10 => ['Out','Outubro'],
        11 => ['Nov','Novembro'],
        12 => ['Dez','Dezembro'],
    ];

    public function listarFeriados($ano = NULL, $formato = NULL) {
        empty($ano) && ($ano = date('Y'));

        $pascoa = easter_date($ano); // Limite de 1970 ou após 2037 da easter_date PHP consulta http://www.php.net/manual/pt_BR/function.easter-date.php
        $dia_pascoa = date('j', $pascoa);
        $mes_pascoa = date('n', $pascoa);
        $ano_pascoa = date('Y', $pascoa);

        $feriados = [
            // Datas Fixas dos feriados Nacionail Basileiras
            'ano_novo' => mktime(0, 0, 0, 1, 1, $ano), // Confraternização Universal - Lei nº 662, de 06/04/49
            'tiradentes' => mktime(0, 0, 0, 4, 21, $ano), // Tiradentes - Lei nº 662, de 06/04/49
            'trabalhador' => mktime(0, 0, 0, 5, 1, $ano), // Dia do Trabalhador - Lei nº 662, de 06/04/49
            'independencia' => mktime(0, 0, 0, 9, 7, $ano), // Dia da Independência - Lei nº 662, de 06/04/49
            'nsaparecida' => mktime(0, 0, 0, 10, 12, $ano), // N. S. Aparecida - Lei nº 6802, de 30/06/80
            'santos' => mktime(0, 0, 0, 11, 2, $ano), // Todos os santos - Lei nº 662, de 06/04/49
            'proclamacao_republica' => mktime(0, 0, 0, 11, 15, $ano), // Proclamação da republica - Lei nº 662, de 06/04/49
            'vespera_natal' => mktime(0, 0, 0, 12, 24, $ano),
            'natal' => mktime(0, 0, 0, 12, 25, $ano), // Natal - Lei nº 662, de 06/04/49
            'vespera_ano_novo' => mktime(0, 0, 0, 12, 31, $ano),
            // These days have a date depending on easter
            '2a_carnaval' => mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 48, $ano_pascoa), //2a feria Carnaval
            '3a_carnaval' => mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 47, $ano_pascoa), //3a feria Carnaval
            '6a_santa' => mktime(0, 0, 0, $mes_pascoa, $dia_pascoa - 2, $ano_pascoa), //6a feira Santa
            'pascoa' => mktime(0, 0, 0, $mes_pascoa, $dia_pascoa, $ano_pascoa), //Pascoa
            'corpus' => mktime(0, 0, 0, $mes_pascoa, $dia_pascoa + 60, $ano_pascoa), //Corpus Cirist
        ];

        if ($formato !== null) {
            foreach ($feriados as $i => $estampaDoTempo) {
                $feriados[$i] = date($formato, $estampaDoTempo);
            }
        }

        return $feriados;
    }

    public function checar($data) {
        !is_int($data) && ($data = strtotime($data));

        return in_array($data, self::listarFeriados(date('Y', $data)));
    }

    public function diasUteisAte($ateData, $deData = null) {
        empty($deData) && ($deData = date('Y-m-d'));
        $estampaDoTempoDe = !is_int($deData) ? strtotime($deData) : $deData;
        $estampaDoTempoAte = !is_int($ateData) ? strtotime($ateData) : $ateData;
        $feriados = self::listarFeriados(date('Y', $estampaDoTempoDe), 'Y-m-d');

        $qtdDiasUteis = 0;
        $qtdDiasCorridos = 0;
        $estampaDoTempoCorrente = $estampaDoTempoDe;

        while ($estampaDoTempoCorrente < $estampaDoTempoAte) {
            $estampaDoTempoCorrente = strtotime('+' . (++$qtdDiasCorridos) . 'day', $estampaDoTempoDe);
            $diaSemana = date('w', $estampaDoTempoCorrente);

            if ($diaSemana != '0' && $diaSemana != '6' && !in_array(date('Y-m-d', $estampaDoTempoCorrente), $feriados)) {
                $qtdDiasUteis++;
            }
        }

        return $qtdDiasUteis;
    }

    public function proximoDiaUtil($data, $qtdDiasUteisAvancar=1) {
        $estampaDoTempo = !is_int($data) ? strtotime($data) : $data;
        $feriados = self::listarFeriados(date('Y', $estampaDoTempo), 'Y-m-d');

        $qtdDiasUteis = 0;
        $qtdDiasCorridos = 0;

        while ($qtdDiasUteis < $qtdDiasUteisAvancar) {
            $estampaDoTempoCorrente = strtotime('+' . (++$qtdDiasCorridos) . 'day', $estampaDoTempo);
            $diaSemana = date('w', $estampaDoTempoCorrente);

            if ($diaSemana != '0' && $diaSemana != '6' && !in_array(date('Y-m-d', $estampaDoTempoCorrente), $feriados)) {
                $qtdDiasUteis++;
            }
        }

        return date('Y-m-d', strtotime('+' . ($qtdDiasUteis) . ' day', $estampaDoTempo));
    }

    public static function anteriorDiaUtil($data, $qtdDiasRetroceder=1) {
        $estampaDoTempo = !is_int($data) ? strtotime($data) : $data;
        $feriados = self::listarFeriados(date('Y', $estampaDoTempo), 'Y-m-d');

        $qtdDiasUteis = 0;
        $qtdDiasCorridos = 0;

        while ($qtdDiasUteis < $qtdDiasRetroceder) {
            $estampaDoTempoCorrente = strtotime('-' . (++$qtdDiasCorridos) . 'day', $estampaDoTempo);
            $diaSemana = date('w', $estampaDoTempoCorrente);

            if ($diaSemana != '0' && $diaSemana != '6' && !in_array(date('Y-m-d', $estampaDoTempoCorrente), $feriados)) {
                $qtdDiasUteis++;
            }
        }

        return date('Y-m-d', strtotime('-' . ($qtdDiasCorridos) . ' day', $estampaDoTempo));
    }
    
    public static function format($date, $format='d/m/Y') {
        $date = trim((string) $date);
        if(!empty($date)) {
            $formated = date($format, strtotime($date));
            return $formated;
        }
        return $date;
    }

}
