<?php

namespace acsp\helpers;

/**
 * @author Bruno Foggia
 */
class Auth {

    public static function checkAccess($actionData, $url = null) {
        if (defined('FORCE_ACCESS') && FORCE_ACCESS === true) {
            return true;
        }

        $actionData = explode('/', preg_replace('/^\//', '', preg_replace('/\/$/', '', $actionData)));
        (count($actionData) < 3) && array_unshift($actionData, '');
        $request = array();
        $request['module'] = $actionData[0];
        $request['controller'] = $actionData[1];
        $request['action'] = count($actionData) >= 3 && !empty($actionData[2]) ? $actionData[2] : 'index';

        return \HiMax\Core::getMe()->checkAccess($request['controller'], $request['action'], $request['module'], empty($url) ? null : $url) === true;
    }

    public static function getUserData($id = NULL, $columns = NULL) {
        empty($columns) && ($columns = ['id', 'nome', 'email']);
        $ci = &\get_instance();
        $shield = \HiMax\Core::getMe();

        if ($id === NULL) {
//            $data = (array) @static::sess_read(\Acsp\GetPermission::getInstance()->getSessionNamespace())['user'];
            $data = array_change_key_case((array)@$shield->getData('user'), CASE_LOWER);
        } else {
            $data = (array) @static::getUsuariosList(['id' => $id], '', $columns)[0];
        }
        return $data;
    }

    public static function getWebserviceToken($user, $pass) {
        $shield = \HiMax\Core::getMe();
        $modelUser = new \acsp\shield\Models\User();

        $userCriteria = [];
        $userCriteria['login'] = $user;
        $userCriteria['novasenha'] = $modelUser->formatPassword($pass);

        $userList = (array) self::getUsuariosList($userCriteria);
        !empty($userList) && $tokenData = count($userList) > 0 ? $shield->getTokenForUser($userList[0]['id']) : null;

        return !empty($tokenData) ? $tokenData['TOKEN'] : null;
    }

    public static function getUsuariosList($criteria = [], $url = '', $fields = NULL) {
        $shield = \HiMax\Core::getMe();
        $list = [];

        $data = [];

        if (array_key_exists('id', $criteria) && empty($criteria['id'])) {
            unset($criteria['id']);
        }
        empty($fields) && ($fields = ['id', 'name', 'email']);
        $data['criteria'] = !empty($criteria) ? $criteria : array();
        if (!empty($url)) {
            $url = explode('/', preg_replace('/^\//', '', $url));
            if(count($url) === 3 && !empty($url[2])) {
                $data['acl'] = array('module' => $url[0], 'class' => $url[1], 'action' => $url[2]);
            } else {
                $data['acl'] = array('module' => '', 'class' => $url[0], 'action' => $url[1]);
            }
        }

        $list = $shield->getUserListByACL($criteria, $fields, (array) @$data['acl']);
        return $list;
    }

    public static function sess_read($name) {
        $ci = &\get_instance();

        if (!empty($ci->session) && is_object($ci->session) && method_exists('userdata', $ci->session)) {
            return $ci->session->userdata($name);
        } else {
            return is_array($_SESSION) && array_key_exists($name, $_SESSION) ? $_SESSION[$name] : null;
        }
    }

}
