
<ul class="menuroot">
    <?php
    $this->load->helper('url');
    $sidemenu = (array)@$this->config->config['modulesInfo']['*']['sidemenu'];
    if (isset($sidemenu)) {
        $url = $this->uri->ruri_string();
        foreach ($sidemenu as $alias => $menu) {
            ?>
            <?php if (isset($menu['child']) && is_array($menu['child'])) { ?>
                <?php if (empty($menu['child'])) { continue; } ?>
                <li class="title <?= empty($menu['closed']) ? '' : 'close' ?>">
                    <h3><?= $alias ?></h3>
                    <ul>
                        <?php
                        foreach ($menu['child'] as $submenu => $submenuData):
                            $value = is_array($submenuData) ? $submenuData['link'] : $submenuData;
                            $value = str_replace('//', '/', $value . '/');
                            
                            $target = '';
                            is_array($submenuData) && (string)@$submenuData['target']==='1' && ($target='_blank');
                            
                            if((string)$this->session->userdata('menulegado')==='S') {
                                $value = preg_replace('/\/index$/', '/grid', $value);
                                $submenuData['realLink'] = preg_replace('/\/index$/', '/grid', $value);
                                $submenuData['legado'] = 1;
                            }
                            
                            if (\acsp\helpers\Auth::checkAccess($value, '/'.$value) === false)
                                continue;
                            
                            
                            $realUrl = is_array($submenuData) && array_key_exists('realLink', $submenuData) ? $submenuData['realLink'] : $value;
                            $realLink = base_url($realUrl);
                            if(is_array($submenuData) && !empty($submenuData['legado'])) {
                                $realLink = str_replace('/modulo/', '/', $realLink);
                            }
                            
                            $selected = strpos($realUrl, $url) !== false ? 'select' : '';
                            ?>
                            <li class="item">
                                <a class='<?= $selected ?>' href="<?= ($realLink) ?>" <?=!empty($target) ? 'target="_blank"' : ''?> <?php if(empty($submenuData['legado']) && empty($target)): ?> data-sk="1"<?php endif; ?>>
                                    <?= $submenu ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </li> 
                <?php } else {
                    $url = !preg_match('/^http/', $menu['link']) ? base_url($menu['link']) : $menu['link'];
                    (string)@$menu['target']==='1' && ($target='_blank');
                ?>
                <li class="item">
                    <h3><a class='<?= isset($menu['active']) ? 'select' : '' ?>' href="<?= $url ?>" <?= empty($menu['target']) ? 'data-sk="1"' : 'target="'.$target.'"' ?> >
                        <?= $alias ?>
                    </a></h3>
                </li> 
                <?php } ?>
        <?php }
    }
    ?>
</ul>