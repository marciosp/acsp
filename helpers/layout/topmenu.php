<nav class="menu container-fluid">
    <ul class="nav-panel pull-left">
        <li id="menu_mobile_show"><a href="#"><i class="fa fa-align-justify"></i></a></li>
        <?php
        global $topmenu;
        $topmenu = [];
        if (is_array($topmenu) && count($topmenu) > 0)
            foreach ($topmenu as $topmenutitle => $topmenuitem):
                ?>
                <li>
                    <a href="<?= $topmenuitem['link'] ?>" <?php if(empty($topmenuitem['legado'])):?> data-sk="1"<?php endif; ?>>
                        <i class="fa <?= $topmenuitem['icon'] ?>"></i> <?= $topmenutitle ?>
                    </a>
                </li>
            <?php endforeach; ?>
    </ul>
    <ul class="nav-panel pull-right" style="padding-left: 0px;">
        <li style="color: #FFF; cursor: pointer;" onclick="var s=$('body').css('font-size');$('body').css('font-size',((parseInt(s.substr(0, s.length-2),10)+2).toString()+'px'));"><i class="fa fa-font "></i><i class="fa fa-chevron-up"></i></li>
        <li>&nbsp;&nbsp;</li>
        <li style="color: #FFF; cursor: pointer;" onclick="var s=$('body').css('font-size');$('body').css('font-size',((parseInt(s.substr(0, s.length-2),10)-2).toString()+'px'));"><i class="fa fa-font "></i><i class="fa fa-chevron-down "></i></li>
    </ul>
</nav>