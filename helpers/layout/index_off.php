<?php
$this->load->helper('url');
$baseUrl = base_url();
$CI = get_instance();
?>
<!DOCTYPE HTML>
<html lang="pt-BR" dir="ltr">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <meta charset="UTF-8">
        <meta name="robots" content="noindex,nofollow">
        <title>ACSP</title>
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/font-awesome/css/font-awesome.min.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/jquery-ui/themes/base/all.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/skiver/skiver.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/dhtmlx/dhtmlx.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/dhtmlx/skins/terrace/dhtmlx.css">
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/bootstrap/dist/css/bootstrap.min.css">
        <!--<link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/DataTables/datatables.min.css">-->
        <link type="text/css" rel="stylesheet" href="<?= $baseUrl ?>bower_components/DataTables/DataTables-1.10.8/css/jquery.dataTables.min.css">
        <!--<link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>public/css/font-awesome/css/font-awesome.min.css">-->
        <!--<link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>public/css/style.css">-->
        <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>bower_components/front-helpers/css/layout.css">
        <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>bower_components/front-helpers/css/acsp.custom.css">
        <link rel="stylesheet" type="text/css" href="<?= $baseUrl ?>public/css/custom.css">
        <script type="text/javascript" src="<?= $baseUrl ?>bower_components/jquery/dist/jquery.min.js"></script>
        <script type="text/javascript" src="<?= $baseUrl ?>bower_components/requirejs/require.js"></script>
        <script type="text/javascript" src="<?= $baseUrl ?>bower_components/front-helpers/js/config-require.js" id="config-require" data-baseurl="<?= base_url('') ?>"></script>
    </head>
    <body>
        <div class="page-header">
            <nav class="info">
                <?= $logininfo ?>
                <?= $infomenu ?>
            </nav>
            <header>
                <div class="titulo pull-left"><a href=""><?= $CI->config->item('modulesInfo')['*']['header'] ?></a></div>
                <div class="logo pull-left">
                    <img class="flecha" src="<?= $baseUrl ?>public/img/flecha.png" alt="">
                    <img class="acsp hidden-xs" src="<?= $baseUrl ?>public/img/logo_pb_acsp.jpg" alt="">
                </div>
            </header>

            <?= $topmenu ?>
        </div>

        <div class="page-body">
            <section id="content">
                <?= $content ?>
            </section>
        </div>

        <script type='text/javascript'>
            require(['public/js/app']);
        </script>
    </body>
</html>